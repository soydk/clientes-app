import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ClienteService } from './clientes/cliente.service';
import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HeaderComponet } from './header/headerComponent';
import { FooterComponent } from './footer/footerComponent';
import { DirectivaComponent } from './directiva/directiva.component';
import { ClientesComponent } from './clientes/clientes.component';
//esta importacion es para las rutas, es decir para las paginas
import { RouterModule, Routes } from '@angular/router';
//ESTE IMPORT NOS PERMITE LA CONEccion con el backend mediante las peticiones HTTP
import { HttpClientModule } from '@angular/common/http';
//desde aqui empezamos con los formularios
import { FormComponent } from './clientes/form.component';
//este import q realizaremos nos sirve para trabjar con formularios, es un import directo de angular
import { FormsModule } from '@angular/forms';
//este import sirve para registrar el locale global y poder cambiar el idioma con el que se formatea las fechas
import { registerLocaleData } from '@angular/common';
import localeES from "@angular/common/locales/es";
import { PaginatorComponent } from './paginator/paginator.component'

import { MatDatepickerModule } from '@angular/material/datepicker'
import { MatNativeDateModule } from '@angular/material/core';
import { DetalleComponent } from './clientes/detalle/detalle.component';
import { LoginComponent } from './userLogin/login.component';



registerLocaleData(localeES, 'es');

//declaramos la constante que contendra nuestras rutas
const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path:  'directiva', component:DirectivaComponent},
  {path: 'clientes', component:ClientesComponent},
  {path: 'clientes/page/:page', component:ClientesComponent},
  {path: 'clientes/form', component:FormComponent},
  {path: 'clientes/form/:id', component:FormComponent},
  {path: 'login', component:LoginComponent},
  //{path: 'clientes/upload/:id', component:DetalleComponent}//esta comentado porque lo usaremos como modal
];


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponet,
    FooterComponent,
    DirectivaComponent,
    ClientesComponent,
    FormComponent,
    PaginatorComponent,
    DetalleComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    FormsModule,
    BrowserAnimationsModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  providers: [ClienteService, {provide: LOCALE_ID, useValue:'es'}],
  bootstrap: [AppComponent]
})
export class AppModule { }
