import { ClienteService } from './cliente.service';
import { Cliente } from './cliente';
import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import { Region } from './region';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.css']
})
export class FormComponent implements OnInit {
  public cliente:Cliente = new Cliente();
  public titulo:string = "Crear Cliente";
  public regiones:Region[];

  public errores_backend: string[];

  constructor(private clienteService:ClienteService,
    private router:Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.cargarCliente();
    this.cargarRegiones();
  }

  public cargarRegiones(): void{
    this.clienteService.getRegiones().subscribe(
      regiones => this.regiones = regiones
    )
  }

  public cargarCliente(): void{
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if(id){
        this.clienteService.getCliente(id).subscribe((response) => this.cliente = response)
      }
    })
  }

  public create(): void{
    this.clienteService.createCliente(this.cliente).subscribe(
      response => {
        this.router.navigate(['/clientes']);
        //respuesta de tipo Json pero devuelta como Cliente
        swal.fire('Nuevo cliente', `Cliente ${response.cliente.nombre} creado con exito!`, 'success');//mensajes en front con sweetalert
        //respuesta obtenida con Json generico apuntando al objeto cliente
        //swal.fire('Nuevo cliente', `Cliente ${response.cliente.nombre} creado con exito!`, 'success');//mensajes en front con sweetalert
      },
      err => {
        this.errores_backend = err.error.Errors as string[];
        console.error('Codigo de error desde el backend: '+ err.status);
        console.error(this.errores_backend);
      }
    );
    console.log("Clicked!")
  }

  public update(): void{
    this.clienteService.updateCliente(this.cliente).subscribe(
      response => {
        this.router.navigate(['/clientes']);
        swal.fire('Cliente Actualizado', `Cliente ${response.cliente.nombre} actualizado con exito!`, 'success')
      },
      err => {
        this.errores_backend = err.error.Errors as string[];
        console.error('Codigo de error desde el backend: '+ err.status);
        console.error(this.errores_backend);
      }
    )
  }

  public compararRegion(regionIterada:Region, regionCliente:Region):boolean{
    if(regionIterada === undefined && regionCliente === undefined){
      return true;
    }
    return regionIterada == null || regionCliente == null? false: regionIterada.id === regionCliente.id;
  }

}
