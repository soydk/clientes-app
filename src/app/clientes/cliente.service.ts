import { Injectable, LOCALE_ID } from "@angular/core";
import { CLIENTES } from "./clientes.json";
import { Cliente } from "./cliente";
import { Observable } from "rxjs";
import { of, throwError } from "rxjs";
import { HttpClient, HttpEvent, HttpHeaders, HttpRequest } from "@angular/common/http";
import { map, catchError } from "rxjs";
import swal from 'sweetalert2';
import { Router } from "@angular/router";
import { formatDate } from "@angular/common";
import { DatePipe } from "@angular/common";
import { Region } from "./region";
import { AuthorizationService } from "../userLogin/authorization.service";



@Injectable()
export class ClienteService{
  private urlEndPoint:string = 'http://localhost:8080/api/clientes'
  private urlEndPointsave:string = 'http://localhost:8080/api/clientes/save'
  private urlEndPointup:string = 'http://localhost:8080/api/cliente/update'
  //URL para regiones
  private urlEndPointRegiones:string = 'http://localhost:8080/api/regiones'

  private httpHeader = new HttpHeaders({'Content-Type' : 'application/json'})

  constructor(private http: HttpClient, private router: Router, private authService:AuthorizationService){}

  //este metodo se lo usara para enviar el tken al backen y asi poder realizar acciones
  private agregarAuthorizationHeader(){
    let token = this.authService.token;
    if(token != null){
      return this.httpHeader.append('Authorization', 'Bearer '+token);
    }
    return this.httpHeader;
  }


  //metodo para saber si esta authenticado
  private isNotAutorizado(e):boolean{
    this.router.navigate(['/login'])
    if(e.status==401 || e.status == 403){
      return true;
    }
    return false;
  }


  //metodo para obtener arreglo de Regiones
  getRegiones(): Observable<Region[]>{
    return this.http.get(this.urlEndPointRegiones).pipe(
      map(regiones => regiones as Region[]),
      catchError(e => {
        this.isNotAutorizado(e);
        return throwError(() => e);
      })
    )
  }

  //metodos
  //consumo de servicio para obtener una lista de clientes como arreglo de clientes
  /*
  getClientes(): Observable<Cliente[]>{
    //return this.http.get<Cliente[]>(this.urlEndPoint);//primera forma de castear a lista de cliente
    //return of(CLIENTES); //de esta forma se retorna una lista quemada en la clase Cliente
    return this.http.get(this.urlEndPoint).pipe(
      map( Response => {
        let clientes = Response as Cliente[];

        return clientes.map( cli => {
          cli.nombre = cli.nombre.toUpperCase();
          //cli.apellido = cli.apellido.toUpperCase(); primera forma de trasnformar a mayuscula
          //cli.createAt = formatDate(cli.createAt, 'EEEE dd/MMMM/yyyy', 'es-ES');
          return cli;
        })
      }));//segunda forma de castear datos
  }*/

  //consumo de servicio para obtener una lista de clientes como json paginado
  getClientes(page:number): Observable<any>{
    return this.http.get(this.urlEndPoint+`/page/${page}`).pipe(
      map( (response:any) => {
         (response.content as Cliente[]).map( cli => {
          cli.nombre = cli.nombre.toUpperCase();
          //cli.apellido = cli.apellido.toUpperCase(); primera forma de trasnformar a mayuscula
          //cli.createAt = formatDate(cli.createAt, 'EEEE dd/MMMM/yyyy', 'es-ES');
          return cli;
        })
        return response;
      }));//segunda forma de castear datos
  }


  //consumo de servicio para guardarun cliente - obtener solo el Cliente proveniente del Json
  /*createCliente(cliente: Cliente): Observable<Cliente>{
    return this.http.post<Cliente>(this.urlEndPointsave, cliente, {headers: this.httpHeader}).pipe(
      map((response: any)=> response.cliente as Cliente),
      catchError(e => {
        swal.fire(e.error.mensaje,e.error.Error, 'error')
        return throwError(()=>e);
      })
    )
  }*/
  createCliente(cliente: Cliente): Observable<any>{//obtener respuesta solo como tipo Json generico
    return this.http.post<any>(this.urlEndPointsave, cliente, {headers: this.agregarAuthorizationHeader()}).pipe(
      catchError(e => {

        if(this.isNotAutorizado(e)){
          return throwError(()=>e);
        }

        if(e.status == 400){
          return throwError(()=>e);
        }

        swal.fire('Error al guardar',e.error.mensaje, 'error')
        return throwError(()=>e);
      })
    )
  }

  //consumo de servicio para consultar un cliente por ID
  getCliente(id: number): Observable<Cliente>{
    return this.http.get<Cliente>(`${this.urlEndPoint}/${id}`, {headers: this.agregarAuthorizationHeader()}).pipe(
      catchError(e => {

        if(this.isNotAutorizado(e)){
          return throwError(()=>e);
        }

        this.router.navigate(['/clientes']);
        swal.fire(e.error.mensaje,e.error.Error, 'error')
        return throwError(()=> e);
      })
    );
  }

  updateCliente(cliente: Cliente): Observable<any>{
    return this.http.put<any>(`${this.urlEndPointup}/${cliente.id}`, cliente, {headers:this.agregarAuthorizationHeader()}).pipe(
      catchError(e => {

        if(this.isNotAutorizado(e)){
          return throwError(()=>e);
        }

        if(e.status == 400){
          return throwError(()=>e);
        }

        swal.fire(e.error.mensaje, e.error.Error, 'error')
        return throwError(()=>e)
      })
    );
  }

  deleteCliente(id:number): Observable<any>{
    return this.http.delete<any>(`http://localhost:8080/api/cliente/delete/${id}`, {headers:this.agregarAuthorizationHeader()}).pipe(
      map((response:any)=> response.mensaje as String),
      catchError(e => {

        if(this.isNotAutorizado(e)){
          return throwError(()=>e);
        }

        swal.fire('Error al eliminar cliente', e.error.mensaje, 'error')
        return throwError(()=>e)
      })
    );
  }

  subirArchivo(archivo: File, id): Observable<HttpEvent<{}>>{
    //se usa para enviar archivos y otros objetos en formato Multipart
    let formData = new FormData();
    formData.append("archivo", archivo);
    formData.append("id", id);

    let httpHeaders = new HttpHeaders();
    let token = this.authService.token;
    if(token != null){
      httpHeaders = httpHeaders.append('Authorization', 'Bearer '+token);
    }

    //usamos este metodo a traves de httprequest para poder obtener el progreso de subida del archivo
    const req = new HttpRequest('POST', `http://localhost:8080/api/cliente/upload`, formData, {reportProgress:true, headers:httpHeaders})
    return this.http.request(req).pipe(
      catchError(e => {
        this.isNotAutorizado(e);
        return throwError(()=>e);
      })
    );
  }

}
