import { Observable } from 'rxjs';
import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  modal_var: boolean = false;
  private _notificarUpload = new EventEmitter<any>();

  constructor() { }

  get notificarUpload(): EventEmitter<any>{
   return this._notificarUpload;
  }


  abrirModal(){
    this.modal_var = true;
  }

  cerrarModal(){
    this.modal_var = false;
  }


}
