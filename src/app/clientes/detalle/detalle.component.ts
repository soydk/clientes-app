import swal from 'sweetalert2';
import { Component, Input, OnInit } from '@angular/core';
import { Cliente } from '../cliente';
import { ClienteService } from '../cliente.service';
import { HttpEventType } from '@angular/common/http';
import { ModalService } from './modal.service';


@Component({
  selector: 'detalle-cliente',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.css']
})
export class DetalleComponent implements OnInit {

  @Input() cliente: Cliente; //este cliente inyectado proviene de clientes_component
  titulo: string = "DETALLE DE CLIENTE";
  existeSeleccion: boolean;
  fotoSeleccionada: File;
  progreso:number = 0;

  constructor(private clienteService: ClienteService, public modalService: ModalService) { }

  ngOnInit(): void {

  }

  obtenerFoto(event){
    this.fotoSeleccionada = event.target.files[0];
    this.progreso = 0;
    console.log(this.fotoSeleccionada);
    if(this.fotoSeleccionada.type.indexOf('image') < 0){
      this.fotoSeleccionada = null;
      swal.fire('Error de Seleccion', 'El archivo debe ser de tipo imagen', 'error')
    }
  }

  subirFoto(){
    if(!this.fotoSeleccionada){
      swal.fire('Error al subir', 'Debe seleccionar una imagen', 'error')
    }else{
      this.clienteService.subirArchivo(this.fotoSeleccionada, this.cliente.id).subscribe(
        event => {
          if(event.type === HttpEventType.UploadProgress){
            this.progreso = Math.round((event.loaded/event.total)*100);
          }else if(event.type === HttpEventType.Response){
            let response:any = event.body;
            this.cliente = response.cliente as Cliente
            //este notificar upload servira para mostrar el cliente actualizado en tiempo real, ejemplo cuando cambiemos una imagen
            this.modalService.notificarUpload.emit(this.cliente);
            swal.fire('Completado', `Foto subida con exito: ${this.cliente.foto}`, 'success')
          }
        }
      )
    }
  }

  cerrarModal(){
    this.modalService.cerrarModal();
    this.fotoSeleccionada = null;
    this.progreso = 0;
  }

}
