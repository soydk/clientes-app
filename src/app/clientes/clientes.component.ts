import swal from 'sweetalert2';
import { ClienteService } from './cliente.service';
import { Component, OnInit } from '@angular/core';
import { Cliente } from './cliente';
import { CLIENTES } from './clientes.json';
import { tap } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { ModalService } from './detalle/modal.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
})
export class ClientesComponent implements OnInit {
  clientes: Cliente[];
  paginador:any;
  //este atributo sera inyectado en detalle component
  clienteSeleccionado: Cliente;

  constructor(
    private clienteService: ClienteService,
    private activatedRoute: ActivatedRoute,
    private modalService: ModalService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe((params) => {
      let page: number = +params.get('page');

      if (!page) {
        page = 0;
      }
      this.clienteService.getClientes(page).pipe(
          tap(response => {
            (response.content as Cliente[]).forEach((cliente) => {
              console.log('Clientes: ' + cliente.nombre);
            });
          })
        ).subscribe(
          response => {
            this.clientes = response.content as Cliente[];
            this.paginador = response;
          }
        );
    });

    // este apartado sirve para que cuando subamos una foto, se actualize automaticamente.
    this.modalService.notificarUpload.subscribe(
      cliente_actualizado => {
        this.clientes = this.clientes.map(cliente_original => {
          if(cliente_actualizado.id == cliente_original.id){
            cliente_original.foto = cliente_actualizado.foto
          }
          return cliente_original;
        })
      }
    )
  }

  delete(cliente: Cliente): void {
    swal
      .fire({
        title: 'Seguro que desea eliminar?',
        text: 'Este proceso no puede revertirse!',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, borrar!',
      })
      .then((result) => {
        if (result.isConfirmed) {
          this.clienteService
            .deleteCliente(cliente.id)
            .subscribe((response) => {
              this.clientes = this.clientes.filter((cli) => cli !== cliente);
              swal.fire('Eliminar!', `${response}`, 'success');
            });
        }
      });
  }

  abrirModal(cliente:Cliente){
    this.clienteSeleccionado = cliente;
    this.modalService.abrirModal();
  }

}
