import { Cliente } from './cliente';
import { Region } from './region';

export const CLIENTES: Cliente[] = [
  {id: 1, nombre:'Darwin', apellido:'Fernandez', email:'rafafrend@gmail.com', createAt:'2023-01-08', foto:'', region: new Region},
  {id: 2, nombre:'Kenia', apellido:'Suarez', email:'keniasuarezpandita@gmail.com', createAt:'2023-01-08',foto:'',region: new Region},
  {id: 3, nombre:'Melany', apellido:'Fernandez', email:'melanie@gmail.com', createAt:'2023-01-08',foto:'',region: new Region},
  {id: 4, nombre:'Gean', apellido:'Fernandez', email:'gean@gmail.com', createAt:'2023-01-08',foto:'',region: new Region},
];
