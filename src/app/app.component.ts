import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Clientes-app';

  curso: string = 'Curso Angular y SpringBoot';
  alumno: string = 'Darwin Fernandez';
}
