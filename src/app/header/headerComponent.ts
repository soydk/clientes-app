import swal from 'sweetalert2';
import { Component } from "@angular/core";
import { AuthorizationService } from "../userLogin/authorization.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './headerComponent.html'
})

export class HeaderComponet {
  curso_udemy: string = 'App Angular Spring'

  constructor(public authService: AuthorizationService, private router:Router){}

  logout():void{
    this.authService.logout();
    swal.fire('Logout', 'Session Cerrada exitosamente', 'success');
    this.router.navigate(['/login']);
  }

}
