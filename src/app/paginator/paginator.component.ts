import { Component, Input, OnInit, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'paginator-nav',
  templateUrl: './paginator.component.html'
})
export class PaginatorComponent implements OnInit, OnChanges {

  @Input() c_paginador:any; //c_paginador es un objeto que contiene el Json donde se encuentra el numero de paginas
  paginas:number[];

  //estas varibles seran el rango calculado de los paginadores
  desde:number;
  hasta:number

  constructor() { }

  ngOnInit(): void {
    this.initPaginator();
  }

  ngOnChanges(change: SimpleChanges): void {

    let paginadorActualizado = change['c_paginador'];

    if(paginadorActualizado.previousValue){
      this.initPaginator();
    }

  }

  private initPaginator():void{
    this.desde = Math.min(Math.max(1, this.c_paginador.number-4), this.c_paginador.totalPages - 5);
    this.hasta = Math.max(Math.min(this.c_paginador.totalPages, this.c_paginador.number + 4), 6);

    if(this.c_paginador.totalPages > 5){
      this.paginas = new Array(this.hasta - this.desde + 1).fill(0).map((_valor, indice) => indice + this.desde);
    }else{
      this.paginas = new Array(this.c_paginador.totalPages).fill(0).map((_valor, indice) => indice +1);
    }
  }


}
