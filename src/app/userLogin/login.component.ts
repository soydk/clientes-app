import { catchError } from 'rxjs';
import { AuthorizationService } from './authorization.service';
import swal from 'sweetalert2';
import { Component, OnInit } from '@angular/core';
import { Usuarios } from './usuarios';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  titulo: string = 'Please Sign In';
  usuario: Usuarios;

  constructor(
    private authSerice: AuthorizationService,
    private router: Router
  ) {
    this.usuario = new Usuarios();
  }

  ngOnInit(): void {
    if(this.authSerice.isAuthenticated()){
      swal.fire('Login', `Bienvenido de nuevo ${this.authSerice.usuario.username}`, 'info');
      this.router.navigate(['/clientes']);
    }
  }

  login(): void {
    console.log(this.usuario);
    if (this.usuario.username == null || this.usuario.password == null) {
      swal.fire('Error Login', 'Username or Passwonrd is Empty', 'error');
      return;
    }



    this.authSerice.login(this.usuario).subscribe({
      next: (response) => {
        console.log(response);

        //creamos metodo para guardar el usuario en session storage
        this.authSerice.almacenarUsuario(response.tokenDeAcceso);
        //creamos metodos para guardar el token en sessionstorage
        this.authSerice.almacenarToken(response.tokenDeAcceso);

        //obtenemos el PAYLOAD del token es decir la informacion del usuario
        let payload = this.authSerice.obtenerDatPayloadToken(response.tokenDeAcceso);
        console.log(payload);
        let usuarioLoguin = this.authSerice.usuario//este metodo es el getUsuario del servicio authService
        this.router.navigate(['/clientes']);
        swal.fire('Login', `Welcome ${usuarioLoguin.username}`, 'success');
      },
    });
  }
}
