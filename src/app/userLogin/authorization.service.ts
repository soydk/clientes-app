import  swal  from 'sweetalert2';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, throwError } from 'rxjs';
import { Usuarios } from './usuarios';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

  private _usuario:Usuarios;
  private _token:string;

  constructor(private http:HttpClient ) { }

  public get usuario():Usuarios{
    if(this._usuario!=null){
      return this._usuario;
    }else if(this._usuario == null && sessionStorage.getItem('usuario') != null){
      this._usuario = JSON.parse(sessionStorage.getItem('usuario')) as Usuarios;
      return this._usuario;
    }else{
      return new Usuarios();
    }
  }

  public get token():string{
    if(this._token != null){
      return this._token;
    }else if(this._token == null && sessionStorage.getItem('token') != null){
      this._token = sessionStorage.getItem('token');
      return this._token;
    }else{
      return null;
    }
  }

  login(usuario:Usuarios):Observable<any>{
    const httpHeaders = new HttpHeaders({'Content-Type' : 'application/json'})
    const urlEnpontaAuth:string = 'http://localhost:8080/api/auth/login';
    return this.http.post<any>(urlEnpontaAuth, usuario, { headers: httpHeaders }).pipe(
      catchError( (e) => {
        swal.fire('Login', 'User or Password Incorrect!', 'error')
        return throwError(()=> e);
      })
    );
  }

  logout():void{
    this._token = null;
    this._usuario = null;
    sessionStorage.clear();
  }

  obtenerDatPayloadToken(accessToken:string): any{
    // 0 para obtener la data TIPO DE CODIFICACION
    // 1 para obtener el payload DATOS DE USUARIO, EXPIRACION Y ROLES
    // 2 para obtener SIGNATURE O FIRMA DEL TOKEN
    if(accessToken != null && accessToken.length>0){
      let payload = JSON.parse(atob(accessToken.split(".")[1])); //decodificamos el token y obtenemos solo el payload
      return payload;
    }
    return null;
  }

  //almacenara el ussuario en el session storage
  almacenarUsuario(accessToken:string): void{
    let payload = this.obtenerDatPayloadToken(accessToken);
    this._usuario = new Usuarios();
    this._usuario.username = payload.sub;
    this._usuario.es_activo = payload.isActivo;
    this._usuario.roles = payload.authorities;
    sessionStorage.setItem('usuario', JSON.stringify(this._usuario));
  }

  almacenarToken(accessToken:string): void{
    this._token = accessToken;
    sessionStorage.setItem('token', accessToken);
  }

  isAuthenticated():boolean{
    let payload = this.obtenerDatPayloadToken(this.token);
    if(payload != null && payload.sub && payload.sub.length>0){
      return true;
    }
    return false;
  }


}
